function createChess(white, black) {
    var value1 = "██  ";
    var value2 = "  ██";
    var odd = "";
    var even = "";
    for (var i = 0; i < white / 2; i++) {
        odd += value2;
        even += value1;
    };

    for (var j = 0; j < black; j++) {
        if (j % 2 != 0) {
            console.log(odd);
        } else {
            console.log(even);
        };
    };
};

createChess(8, 8);